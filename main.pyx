import cython

cdef int a(int c):
    return c


def s(int N):
    cdef int ac = 0
    cdef int i = 0
    for i in range(N):
        ac = a(ac) + a(i)
    return ac

# def aa(c):
#     return c

# def ss(N):
#     ac = 0
#     i = 0
#     for i in range(N):
#         ac = aa(ac) + aa(i)
#     return ac

# ss(20000000)
